FROM node:10-alpine as builder
LABEL maintainer="HcgRandon <me@randon.moe>"

RUN apk add --no-cache git

COPY . /build
WORKDIR /build

# Build typescrupt project
RUN yarn install && \
	yarn run build

FROM node:10-alpine
# install required packages
RUN apk add --no-cache git

# create stat user
RUN adduser -Sh /stats stats

# copy raw source & built project
COPY . /stats
COPY --from=builder /build/build /stats/build

# fix perms
RUN chown -R stats /stats

USER stats
WORKDIR /stats

# Install for prod
RUN yarn install --production && \
    yarn cache clean

CMD ["node", "/stats/build/index.js"]
