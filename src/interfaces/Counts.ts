export interface Counts {
	guilds: number;
	users: number;
	streams: number;
	[key: string]: number;
}
