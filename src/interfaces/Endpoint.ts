import { Counts } from './Counts';

export interface Endpoint {
	url(id: string): string;
	headers?(): { [key: string]: string };
	body?(counts: Counts): { [key: string]: string | number };
}
