import { Application } from '@ayanaware/bento';

import { Logger } from '@ayanaware/logger';

const log = Logger.get();

const app = new Application();
(async () => {
	await app.start();
	await app.verify();
})().catch(e => {
	log.error(`Instructions unclear. Dick caught in ceiling fan, ${e}`);
	process.exit(1);
});

