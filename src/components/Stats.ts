import axios from 'axios';

import { Counts } from '../interfaces/Counts';
import { Endpoint } from '../interfaces/Endpoint';

import { ComponentAPI, Variable } from '@ayanaware/bento';
import { Variables } from '../Variables';

import { Logger } from '@ayanaware/logger';
const log = Logger.get();
export class Stats {
	public api: ComponentAPI;
	public name: string = 'Stats';

	@Variable({ name: Variables.API_URI })
	private uri: string;

	@Variable({ name: Variables.BOT_ID })
	private botId: string;

	private endpoints: Endpoint[] = [];
	private interval: NodeJS.Timeout;

	private userAgent = 'AyanaStats';
	
	public async onLoad() {
		// Add version to userAgent
		const { version } = require('../../package.json');
		this.userAgent = `${this.userAgent}/${version}`;

		// Endpoints

		// bots.ondiscord.xyz
		const bodToken = this.api.getVariable<string>({ name: Variables.BOTSONDISCORD_TOKEN, default: null });
		if (bodToken) {
			this.endpoints.push({
				url: (id: string) => `https://bots.ondiscord.xyz/bot-api/bots/${id}/guilds`,
				headers: () => ({ Authorization: bodToken }),
				body: (counts: Counts) => ({
					guildCount: counts.guilds,
				}),
			});
		}

		// discord.bots.gg
		const botsggToken = this.api.getVariable<string>({ name: Variables.DISCORDBOTSGG_TOKEN, default: null });
		if (botsggToken) {
			this.endpoints.push({
				url: (id: string) => `https://discord.bots.gg/api/v1/bots/${id}/stats`,
				headers: () => ({ Authorization: botsggToken }),
				body: (counts: Counts) => ({
					guildCount: counts.guilds,
				}),
			});
		}

		// top.gg
		const topggToken = this.api.getVariable<string>({ name: Variables.TOPGG_TOKEN, default: null });
		if (topggToken) {
			this.endpoints.push({
				url: (id: string) => `https://top.gg/api/bots/${id}/stats`,
				headers: () => ({ Authorization: topggToken }),
				body: (counts: Counts) => ({
					server_count: counts.guilds
				}),
			});
		}

		if (this.endpoints.length < 1) throw new Error(`No endpoints`);
		log.info(`Ready! Posting to ${this.endpoints.length} endpoints.`);

		// do inital run
		await this.run();

		// parse & start interval
		const minsRaw = this.api.getVariable<string>({ name: Variables.INTERVAL_MINS, default: '5' });
		const mins = Number(minsRaw) ?? 5;

		log.info(`Posting stats every ${mins} minutes.`);
		this.interval = setInterval(() => {
			this.run().catch(e => {
				log.error(`Run Failure: ${e}`);
				console.error(e);
			});
		}, mins * 60 * 1000);
	}

	public async onUnload() {
		clearInterval(this.interval);
	}

	public async run() {
		// fetch updated counts
		log.info(`Fetching counts: ${this.uri}`);
		const r = await axios.get<{ total: Counts }>(this.uri);
		if (r.status !== 200) throw new Error(`API: ${this.uri} non 200 status`);
		if (!r.data.total) throw new Error('API: No total object found');

		const counts = r.data.total;
		log.info(`Counts: ${JSON.stringify(counts)}`);

		// guild count zero, hold update for now
		if (counts.guilds === 0) {
			log.warn(`Skipping count update... 0 Guilds`);
			return;
		}

		// post stats
		let success = 0;
		for (const endpoint of this.endpoints) {
			const url = endpoint.url(this.botId);

			let headers = {
				'Content-Type': 'application/json',
				'User-Agent': this.userAgent,
			};

			if (endpoint.headers) {
				headers = Object.assign({}, headers, endpoint.headers());
			}

			let body = {};
			if (endpoint.body) {
				body = Object.assign({}, body, endpoint.body(counts));
			}

			try {
				await axios.post(url, body, {
					headers,
				});

				success++;
			} catch (e) {
				log.error(`Error posting stats to "${url}". ${e}`);
			}
		}

		if (success > 0) log.info(`Sucessfully posted update to ${success} endpoints`);
	}
}
